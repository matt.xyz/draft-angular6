import { ComponentFixture, TestBed } from '@angular/core/testing';

import { KuromiComponent } from './kuromi.component';

describe('KuromiComponent', () => {
  let component: KuromiComponent;
  let fixture: ComponentFixture<KuromiComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ KuromiComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(KuromiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
