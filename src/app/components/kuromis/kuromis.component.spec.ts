import { ComponentFixture, TestBed } from '@angular/core/testing';

import { KuromisComponent } from './kuromis.component';

describe('KuromisComponent', () => {
  let component: KuromisComponent;
  let fixture: ComponentFixture<KuromisComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ KuromisComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(KuromisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
